﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Cliente
{
   public class ClienteDataBase
    {
        public int Salvar(ClienteDTO cliente)
        {
            string script = @"INSERT INTO tb_cliente(                  
	               nm_cliente,
                   ds_telefone,
                   ds_email,
                   ds_tellCelular,
	               ds_cpf,
	               id_endereco)
                   VALUES(
	               @nm_cliente,
                   @ds_telefone,
                   @ds_tellCelular,
                   @ds_email,
	               @ds_cpf,
	               @id_endereco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("ds_telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_tellCelular", cliente.Celular));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_cpf", cliente.CPF));
            parms.Add(new MySqlParameter("id_endereco", cliente.idEndereco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(ClienteDTO cliente)
        {
            string script = @"UPDATE tb_cliente SET nm_cliente = @nm_cliente,
                                                    ds_telefone = @ds_telefone,
                                                    ds_tellCelular = @ds_tellCelular,
                                                    ds_email = @ds_email,
	                                                ds_cpf = @ds_cpf,
	                                                id_endereco = @id_endereco,
                                                 WHERE id_dliente = @id_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", cliente.Nome));
            parms.Add(new MySqlParameter("ds_telefone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_tellCelular", cliente.Celular));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_cpf", cliente.CPF));
            parms.Add(new MySqlParameter("id_endereco", cliente.idEndereco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idCliente)
        {
            string script = @"DELETE FROM tb_cliente WHERE id_cliente = @id_cliente";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", idCliente));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT FROM tb_cliente";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ClienteDTO> cliente = new List<ClienteDTO>();
            while (reader.Read()) 
            {
                ClienteDTO DTO = new ClienteDTO();
                DTO.Id = reader.GetInt32("id_cliente");
                DTO.Nome = reader.GetString("nm_cliente");
                DTO.Telefone = reader.GetString("ds_telefone");
                DTO.Celular = reader.GetString("ds_tellCelular");
                DTO.Email = reader.GetString("ds_email");
                DTO.CPF = reader.GetString("ds_cpf");
                DTO.idEndereco = reader.GetInt32("id_endereco");

                cliente.Add(DTO);
            }
            reader.Close();
            return cliente;
        }
    }
}
