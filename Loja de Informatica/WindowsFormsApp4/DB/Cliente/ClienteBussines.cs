﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Cliente
{
    public class ClienteBussines
    {
        public int Salvar(ClienteDTO cliente)
        {
            ClienteDataBase clientedb = new ClienteDataBase();
            int id = clientedb.Salvar(cliente);
            return id;
        }
        public void Alterar(ClienteDTO cliente)
        {
            ClienteDataBase clientedb = new ClienteDataBase();
            clientedb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            ClienteDataBase clientedb = new ClienteDataBase();
            clientedb.Remover(idcliente);
        }
        public List<ClienteDTO> Listar()
        {
            ClienteDataBase clientedb = new ClienteDataBase();
            List<ClienteDTO> cliente = clientedb.Listar();
            return cliente;
        }
    }
}
