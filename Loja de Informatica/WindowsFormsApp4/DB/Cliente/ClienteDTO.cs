﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Cliente
{
    public class ClienteDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }       
        public string Telefone { get; set; }
        public string CPF { get; set; }
        public int idEndereco { get; set; }
        public string Email { get; set; }
        public string Celular { get; set; }

    }
}
