﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Orçamento
{
   public class OrcamentoDatabase
    {
        public int Salvar(OrcamentoDTO cliente)
        {
            string script = @"INSERT INTO tb_orcamento(                  
	               nm_nome,
                   ds_email,
                   ds_celular,
                   ds_telfone,
                   vl_valor
	               )
                   VALUES(
	               @nm_nome,
                   @ds_email,
                   @ds_telfone,
                   @ds_celular,
                   @vl_valor
	             )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente.Nome));
            parms.Add(new MySqlParameter("ds_telfone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_celular", cliente.Celular));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_valor", cliente.Valor));
          
           
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(OrcamentoDTO cliente)
        {
            string script = @"UPDATE tb_orcamento SET nm_nome = @nm_nome,
                                                    ds_telfone = @ds_telfone,
                                                    ds_celular = @ds_celular,
                                                    ds_email = @ds_email,
	                                                vl_valor = @vl_valor
                            ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", cliente.Nome));
            parms.Add(new MySqlParameter("ds_telfone", cliente.Telefone));
            parms.Add(new MySqlParameter("ds_celular", cliente.Celular));
            parms.Add(new MySqlParameter("ds_email", cliente.Email));
            parms.Add(new MySqlParameter("ds_valor", cliente.Valor));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idCliente)
        {
            string script = @"DELETE FROM tb_orcamento WHERE id_orcamento = @id_orcamento";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_orcamento", idCliente));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<OrcamentoDTO> Listar()
        {
            string script = @"SELECT FROM tb_orcamento";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<OrcamentoDTO> cliente = new List<OrcamentoDTO>();
            while (reader.Read())
            {
                OrcamentoDTO DTO = new OrcamentoDTO();
                DTO.ID = reader.GetInt32("id_orcamento");
                DTO.Nome = reader.GetString("nm_nome");
                DTO.Telefone = reader.GetString("ds_telfone");
                DTO.Celular = reader.GetString("ds_celular");
                DTO.Email = reader.GetString("ds_email");
                DTO.Valor = reader.GetString("ds_valor");

                cliente.Add(DTO);
            }
            reader.Close();
            return cliente;
        }
    }
}
