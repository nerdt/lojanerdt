﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Orçamento
{
    public class OrcamentoBussines
    {
        public int Salvar(OrcamentoDTO orcamento)
        {
            OrcamentoDatabase deptodb = new OrcamentoDatabase();
            int id = deptodb.Salvar(orcamento);
            return id;
        }
        public void Alterar(OrcamentoDTO orcamento)
        {
            OrcamentoDatabase deptodb = new OrcamentoDatabase();
            deptodb.Alterar(orcamento);
        }
        public void Remove(int idcliente)
        {
            OrcamentoDatabase deptodb = new OrcamentoDatabase();
            deptodb.Remover(idcliente);
        }
        public List<OrcamentoDTO> Listar()
        {
            OrcamentoDatabase deptodb = new OrcamentoDatabase();
            List<OrcamentoDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
