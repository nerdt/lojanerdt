﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;
using WindowsFormsApp4.DB.Endereço;

namespace WindowsFormsApp4.DB.SupTecnico
{
    class SupTecnicoDataBase
    {
        public int Salvar(SupTecnicoDTO supTecnico)
        {
            string script = @"INSERT INTO tb_supTecnico(id_cliente,
	              id_funcionario)
                   VALUES (@id_cliente,
	               @id_funcionario)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", supTecnico.IdCliente));
            parms.Add(new MySqlParameter("id_funcionario", supTecnico.IdFuncionario));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(SupTecnicoDTO supTecnico)
        {
            string script = @"UPDATE tb_supTecnico SET  id_cliente = @id_cliente
	                                                    id_funcionario = @id_funcionario
	                                                  
	                                               
                                                 WHERE id_supTecnico = @id_supTecnico";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", supTecnico.IdCliente));
            parms.Add(new MySqlParameter("id_funcionario", supTecnico.IdFuncionario));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idsuptec)
        {
            string script = @"DELETE FROM tb_supTecnico WHERE id_supTecnico @id_supTecnico";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_supTecnico", idsuptec));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<SupTecnicoDTO> Listar()
        {
            string script = @"SELECT FROM tb_supTecnico";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<SupTecnicoDTO> endereco = new List<SupTecnicoDTO>();
            while (reader.Read())
            {
                SupTecnicoDTO DTO = new SupTecnicoDTO();
                DTO.Id = reader.GetInt32("id_supTecnico");
                DTO.IdCliente = reader.GetInt32("id_clinte");
                DTO.IdFuncionario = reader.GetInt32("id_funcionario");
                
                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}
