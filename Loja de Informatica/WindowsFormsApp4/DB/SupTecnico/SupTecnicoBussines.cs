﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.SupTecnico
{
    class SupTecnicoBussines
    {
        public int Salvar(SupTecnicoDTO cliente)
        {
            SupTecnicoDataBase deptodb = new SupTecnicoDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(SupTecnicoDTO cliente)
        {
            SupTecnicoDataBase deptodb = new SupTecnicoDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            SupTecnicoDataBase deptodb = new SupTecnicoDataBase();
            deptodb.Remover(idcliente);
        }
        public List<SupTecnicoDTO> Listar()
        {
            SupTecnicoDataBase deptodb = new SupTecnicoDataBase();
            List<SupTecnicoDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
