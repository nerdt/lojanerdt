﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Endereço
{
    class EndereçoBussines
    {
        public int Salvar(EndereçoDTO cliente)
        {
           EndereçoDataBase deptodb = new EndereçoDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(EndereçoDTO cliente)
        {
            EndereçoDataBase deptodb = new EndereçoDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            EndereçoDataBase deptodb = new EndereçoDataBase();
            deptodb.Remover(idcliente);
        }
        public List<EndereçoDTO> Listar()
        {
            EndereçoDataBase deptodb = new EndereçoDataBase();
            List<EndereçoDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
