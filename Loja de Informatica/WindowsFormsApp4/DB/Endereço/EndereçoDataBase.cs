﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;
using WindowsFormsApp4.DB.Departamento;

namespace WindowsFormsApp4.DB.Endereço
{
    class EndereçoDataBase
    {
        public int Salvar(EndereçoDTO endereco)
        {
            string script = @"INSERT INTO tb_endereco(
                  nm_estado,
	              nm_cidade,
	              ds_cep)
                  VALUES (@nm_estado,
                   @nm_cidade,
                   @ds_cep)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_estado", endereco.Estado));
            parms.Add(new MySqlParameter("nm_cidade", endereco.Cidade));
            parms.Add(new MySqlParameter("ds_cep", endereco.CEP));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(EndereçoDTO endereco)
        {
            string script = @"UPDATE tb_endereco SET  nm_estado = @nm_estado,
	                                                  nm_cidade = @nm_cidade,
	                                                  ds_cep = @ds_cep,
	                                              WHERE id_endereco = @id_endereco";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_estado", endereco.Estado));
            parms.Add(new MySqlParameter("nm_cidade", endereco.Cidade));
            parms.Add(new MySqlParameter("ds_cep", endereco.CEP));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idendereco)
        {
            string script = @"DELETE FROM tb_endereco WHERE id_endereco = @id_endereco";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_endereco", idendereco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<EndereçoDTO> Listar()
        {
            string script = @"SELECT FROM tb_endereco";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<EndereçoDTO> endereco = new List<EndereçoDTO>();
            while (reader.Read())
            {
                EndereçoDTO DTO = new EndereçoDTO();
                DTO.Id = reader.GetInt32("id_depto");
                DTO.Estado = reader.GetString("nm_estado");
                DTO.Cidade = reader.GetString("nm_cidade");
                DTO.CEP = reader.GetString("ds_cep");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}
