﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Peças
{
    class PeçasBussines
    {
        public int Salvar(PeçasDTO cliente)
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(PeçasDTO cliente)
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            deptodb.Remover(idcliente);
        }
        public List<PeçasDTO> Listar()
        {
            PeçasDataBase deptodb = new PeçasDataBase();
            List<PeçasDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
