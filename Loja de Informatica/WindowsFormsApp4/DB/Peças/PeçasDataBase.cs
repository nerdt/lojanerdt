﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Peças
{
    class PeçasDataBase
    {
        public int Salvar(PeçasDTO pecas)
        {
            string script = @"INSERT INTO tb_pecas(nm_peca,
                  vl_peca,
	              ds_peca)
                   BVALUES (@nm_peca,
                  @vl_peca,
	              @ds_peca)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_peca", pecas.Nome));
            parms.Add(new MySqlParameter("vl_peca", pecas.Valor));
            parms.Add(new MySqlParameter("ds_peca", pecas.Descricao));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(PeçasDTO pecas)
        {
            string script = @"UPDATE tb_pecas SET  nm_peca = @nm_peca
                                                      vl_peca = @vl_peca
	                                                  ds_peca = @ds_peca
                   
	                                               
                                                 WHERE id_pecas = @id_pecas";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_peca", pecas.Nome));
            parms.Add(new MySqlParameter("vl_peca", pecas.Valor));
            parms.Add(new MySqlParameter("ds_peca", pecas.Descricao));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idpecas)
        {
            string script = @"DELETE FROM tb_pecas WHERE  id_pecas @ id_pecas";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter(" id_pecas", idpecas));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<PeçasDTO> Listar()
        {
            string script = @"SELECT FROM tb_pecas";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PeçasDTO> endereco = new List<PeçasDTO>();
            while (reader.Read())
            {
                PeçasDTO DTO = new PeçasDTO();
                DTO.Id = reader.GetInt32(" id_pecas");
                DTO.Nome = reader.GetString("nm_peca");
                DTO.Valor = reader.GetDecimal("vl_peca");
                DTO.Descricao = reader.GetString("ds_peca");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}
