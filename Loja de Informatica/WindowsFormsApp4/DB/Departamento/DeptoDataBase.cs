﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;
using WindowsFormsApp4.DB.Cliente;

namespace WindowsFormsApp4.DB.Departamento
{
   public class DeptoDataBase
    {
        public int Salvar(Departamento.DeptoDTO depto)
        {
            string script = @"INSERT INTO tb_depto(
                   nm_depto,
                   ds_local)
                   VALUES(	             
	               @nm_depto,
                   @ds_local)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_depto",depto.Nome));
            parms.Add(new MySqlParameter("ds_local",depto.Local));
            
            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(DeptoDTO depto)
        {
            string script = @"UPDATE tb_depto SET  nm_depto = @nm_depto
                                                      ds_local = @ds_local
	                                               
                                                 WHERE id_depto = @id_depto";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_depto", depto.Nome));
            parms.Add(new MySqlParameter("ds_local", depto.Local));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int iddepto)
        {
            string script = @"DELETE FROM tb_depto WHERE id_depto = @id_depto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_depto", iddepto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<DeptoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_depto";
            Database db = new Database();

            List<MySqlParameter> parms = new List<MySqlParameter>(); 

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DeptoDTO> depto = new List<DeptoDTO>();
            while (reader.Read())
            {
                DeptoDTO dto = new DeptoDTO();
                dto.Id = reader.GetInt32("id_depto");
                dto.Nome = reader.GetString("nm_depto");
                dto.Local = reader.GetString("ds_local");

                depto.Add(dto);
            }
            reader.Close();

            return depto;
        }
    }
}
