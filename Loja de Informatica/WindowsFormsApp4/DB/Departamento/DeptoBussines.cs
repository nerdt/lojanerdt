﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Departamento
{
    class DeptoBussines
    {
        public int Salvar(DeptoDTO cliente)
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(DeptoDTO cliente)
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            deptodb.Remover(idcliente);
        }
        public List<DeptoDTO> Listar()
        {
            DeptoDataBase deptodb = new DeptoDataBase();
            List<DeptoDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}

