CREATE TABLE `tb_cliente` (
	`id_cliente` INT NOT NULL AUTO_INCREMENT,
	`nm_cliente` varchar(50) NOT NULL,
	`ds_telefone` varchar(20) NOT NULL,
	`ds_tellCelular` varchar(20) NOT NULL,
	`ds_email` varchar(150) NOT NULL,
	`ds_cpf` varchar(20) NOT NULL,
	`id_endereco` INT NOT NULL,
	PRIMARY KEY (`id_cliente`)
);

CREATE TABLE `tb_endereco` (
	`id_endereco` INT NOT NULL AUTO_INCREMENT,
	`nm_estado` varchar(2) NOT NULL,
	`nm_cidade` varchar(20) NOT NULL,
	`ds_cep` varchar(15) NOT NULL,
	`ds_complemento` varchar(50) NOT NULL,
	PRIMARY KEY (`id_endereco`)
);

CREATE TABLE `tb_funcionario` (
	`id_funcionario` INT NOT NULL AUTO_INCREMENT,
	`nm_funcionario` varchar(50) NOT NULL,
	`ds_telefone` varchar(15) NOT NULL,
	`ds_email` varchar(50) NOT NULL,
	`ds_cpf` varchar(20) NOT NULL,
	`id_depto` INT NOT NULL,
	`id_endereco` INT NOT NULL,
	PRIMARY KEY (`id_funcionario`)
);

CREATE TABLE `tb_fornecedor` (
	`id_fornecedor` INT NOT NULL AUTO_INCREMENT,
	`nm_fornecedor` varchar(50) NOT NULL,
	`ds_cnpj` varchar(20) NOT NULL,
	`id_endereco` INT NOT NULL,
	PRIMARY KEY (`id_fornecedor`)
);

CREATE TABLE `tb_supTecnico` (
	`id_supTecnico` INT NOT NULL AUTO_INCREMENT,
	`id_cliente` INT NOT NULL,
	`id_funcionario` INT NOT NULL,
	`dt_suporte` DATE NOT NULL,
	PRIMARY KEY (`id_supTecnico`)
);

CREATE TABLE `tb_pecas` (
	`id_pecas` INT NOT NULL AUTO_INCREMENT,
	`nm_peca` varchar(50) NOT NULL,
	`vl_peca` DECIMAL(5,2) NOT NULL,
	`ds_peca` varchar(100) NOT NULL,
	PRIMARY KEY (`id_pecas`)
);

CREATE TABLE `tb_supPecas` (
	`id_supPecas` INT NOT NULL AUTO_INCREMENT,
	`id_pecas` INT NOT NULL,
	`id_supTecnico` INT NOT NULL,
	PRIMARY KEY (`id_supPecas`)
);

CREATE TABLE `tb_estoque` (
	`id_estoque` INT NOT NULL AUTO_INCREMENT,
	`id_pecas` INT NOT NULL,
	`id_fornecedor` INT NOT NULL,
	`nr_qtd` INT NOT NULL,
	PRIMARY KEY (`id_estoque`)
);

CREATE TABLE `tb_depto` (
	`id_depto` INT NOT NULL AUTO_INCREMENT,
	`nm_depto` varchar(50) NOT NULL AUTO_INCREMENT,
	`ds_local` varchar(50) NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (`id_depto`)
);

ALTER TABLE `tb_cliente` ADD CONSTRAINT `tb_cliente_fk0` FOREIGN KEY (`id_endereco`) REFERENCES `tb_endereco`(`id_endereco`);

ALTER TABLE `tb_funcionario` ADD CONSTRAINT `tb_funcionario_fk0` FOREIGN KEY (`id_depto`) REFERENCES `tb_depto`(`id_depto`);

ALTER TABLE `tb_funcionario` ADD CONSTRAINT `tb_funcionario_fk1` FOREIGN KEY (`id_endereco`) REFERENCES `tb_endereco`(`id_endereco`);

ALTER TABLE `tb_fornecedor` ADD CONSTRAINT `tb_fornecedor_fk0` FOREIGN KEY (`id_endereco`) REFERENCES `tb_endereco`(`id_endereco`);

ALTER TABLE `tb_supTecnico` ADD CONSTRAINT `tb_supTecnico_fk0` FOREIGN KEY (`id_cliente`) REFERENCES `tb_cliente`(`id_cliente`);

ALTER TABLE `tb_supTecnico` ADD CONSTRAINT `tb_supTecnico_fk1` FOREIGN KEY (`id_funcionario`) REFERENCES `tb_funcionario`(`id_funcionario`);

ALTER TABLE `tb_supPecas` ADD CONSTRAINT `tb_supPecas_fk0` FOREIGN KEY (`id_pecas`) REFERENCES `tb_pecas`(`id_pecas`);

ALTER TABLE `tb_supPecas` ADD CONSTRAINT `tb_supPecas_fk1` FOREIGN KEY (`id_supTecnico`) REFERENCES `tb_supTecnico`(`id_supTecnico`);

ALTER TABLE `tb_estoque` ADD CONSTRAINT `tb_estoque_fk0` FOREIGN KEY (`id_pecas`) REFERENCES `tb_pecas`(`id_pecas`);

ALTER TABLE `tb_estoque` ADD CONSTRAINT `tb_estoque_fk1` FOREIGN KEY (`id_fornecedor`) REFERENCES `tb_fornecedor`(`id_fornecedor`);
