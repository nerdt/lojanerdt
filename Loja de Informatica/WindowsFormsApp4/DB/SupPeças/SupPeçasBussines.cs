﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.SupPeças
{
    class SupPeçasBussines
    {
        public int Salvar(SupPeçasDTO cliente)
        {
            SupPeçasDataBase deptodb = new SupPeçasDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(SupPeçasDTO cliente)
        {
            SupPeçasDataBase deptodb = new SupPeçasDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            SupPeçasDataBase deptodb = new SupPeçasDataBase();
            deptodb.Remover(idcliente);
        }
        public List<SupPeçasDTO> Listar()
        {
            SupPeçasDataBase deptodb = new SupPeçasDataBase();
            List<SupPeçasDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
