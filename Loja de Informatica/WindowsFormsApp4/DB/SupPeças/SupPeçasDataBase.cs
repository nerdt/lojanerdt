﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;
using WindowsFormsApp4.DB.Endereço;

namespace WindowsFormsApp4.DB.SupPeças
{
    class SupPeçasDataBase
    {
        public int Salvar(SupPeçasDTO suppeca)
        {
            string script = @"INSERT INTO tb_supPecas(id_pecas,
	              id_supTecnico)
                   VALUES (@id_pecas,
	              @id_supTecnico)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pecas", suppeca.Idpeca));
            parms.Add(new MySqlParameter("id_supTecnico", suppeca.IdSupTecnico));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(SupPeçasDTO suppeca)
        {
            string script = @"UPDATE tb_supPecas SET   id_pecas = @id_pecas
	                                                 id_supTecnico = @supTecnico
	                                               
                                                 WHERE id_supPecas = @id_supPecas";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pecas", suppeca.Idpeca));
            parms.Add(new MySqlParameter("id_supTecnico", suppeca.IdSupTecnico));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idsuppeca)
        {
            string script = @"DELETE FROM tb_supPecas WHERE id_supPecas @id_supPecas";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_supPecas", idsuppeca));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<SupPeçasDTO> Listar()
        {
            string script = @"SELECT FROM tb_supPecas";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<SupPeçasDTO> endereco = new List<SupPeçasDTO>();
            while (reader.Read())
            {
                SupPeçasDTO DTO = new SupPeçasDTO();
                DTO.Id = reader.GetInt32("id_supPecas");
                DTO.Idpeca = reader.GetInt32("id_pecas");
                DTO.IdSupTecnico = reader.GetInt32("id_supTecenico");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}
