﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Fornecedor
{
    class FornecedorBussines
    {
        public int Salvar(FornecedorDTO cliente)
        {
           FornecedorDataBase deptodb = new FornecedorDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(FornecedorDTO cliente)
        {
            FornecedorDataBase deptodb = new FornecedorDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            FornecedorDataBase deptodb = new FornecedorDataBase();
            deptodb.Remover(idcliente);
        }
        public List<FornecedorDTO> Listar()
        {
            FornecedorDataBase deptodb = new FornecedorDataBase();
            List<FornecedorDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
