﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Fornecedor
{
    class FornecedorDataBase
    {
        public int Salvar(FornecedorDTO forn)
        {
            string script = @"INSERT INTO tb_fornecedor( id_fornecedor,
	             nm_fornecedor,
	             ds_cnpj,
	             id_endereco)
                 VALUES (@nm_fornecedor,
	             @ds_cnpj,
	             @id_endereco)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", forn.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", forn.CNPJ));
            parms.Add(new MySqlParameter("id_endereco", forn.IdEndereco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(FornecedorDTO forn)
        {
            string script = @"UPDATE tb_fornecedor SET  nm_fornecedor = @nm_forncedor
	                                                    ds_cnpj = @ds_cnpj
	                                                    id_endereco = @id_endereco
	                                               
                                                 WHERE id_fornecedor = @id_fornecedor";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_fornecedor", forn.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", forn.CNPJ));
            parms.Add(new MySqlParameter("id_endereco", forn.IdEndereco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idforn)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_fornecedor @id_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_fornecedor", idforn));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT FROM tb_fornecedor";
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List< FornecedorDTO> endereco = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO DTO = new FornecedorDTO();
                DTO.Id = reader.GetInt32("id_fornecedor");
                DTO.Nome = reader.GetString("nm_fornecedor");
                DTO.CNPJ = reader.GetString("ds_cnpj");
                DTO.IdEndereco = reader.GetInt32("id_endereco");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}
