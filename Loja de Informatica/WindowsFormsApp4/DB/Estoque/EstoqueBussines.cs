﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Estoque
{
    class EstoqueBussines
    {
        public int Salvar(EstoqueDTO estoque)
        {
            EstoqueDataBase deptodb = new EstoqueDataBase();
            int id = deptodb.Salvar(estoque);
            return id;
        }
        public void Alterar(EstoqueDTO estoque)
        {
            EstoqueDataBase deptodb = new EstoqueDataBase();
            deptodb.Alterar(estoque);
        }
        public void Remove(int idcliente)
        {
            EstoqueDataBase deptodb = new EstoqueDataBase();
            deptodb.Remover(idcliente);
        }
        public List<EstoqueDTO> Listar()
        {
            EstoqueDataBase deptodb = new EstoqueDataBase();
            List<EstoqueDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
