﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Estoque
{
   public class EstoqueDTO
    {
        public int Id { get; set; }
        public int IdPeças { get; set; }
        public int IdFornecedor { get; set; }
        public int Quantidade { get; set; }
    }
}
