﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApp4.DB.Base;

namespace WindowsFormsApp4.DB.Estoque
{
    class EstoqueDataBase
    {
        public int Salvar(EstoqueDTO estoque)
        {
            string script = @"INSERT INTO tb_estoque(id_pecas,
	              id_fornecedor,
	              ds_qtd)
                  VALUES (@id_pecas
	              @id_fornecedor,
	              @ds_qtd)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pecas", estoque.IdPeças));
            parms.Add(new MySqlParameter("id_fornecedor", estoque.IdFornecedor));
            parms.Add(new MySqlParameter("ds_qtd", estoque.Quantidade));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }
        public void Alterar(EstoqueDTO estoque)
        {
            string script = @"UPDATE tb_estoque SET  nm_estado = @nm_estado
	                                                  nm_cidade = @nm_cidade
	                                                  ds_cep = @ds_cep
	                                               
                                                 WHERE id_estoque = @id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pecas", estoque.IdPeças));
            parms.Add(new MySqlParameter("id_fornecedor", estoque.IdFornecedor));
            parms.Add(new MySqlParameter("ds_qtd", estoque.Quantidade));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Remover(int idestoque)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque @id_estoque";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_endereco", idestoque));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<EstoqueDTO> Listar()
        {
            string script = @"SELECT FROM tb_estoque";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<EstoqueDTO> endereco = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO DTO = new EstoqueDTO();
                DTO.Id = reader.GetInt32("id_estoque");
                DTO.IdPeças = reader.GetInt32("id_pecas");
                DTO.IdFornecedor = reader.GetInt32("id_fornecedor");
                DTO.Quantidade = reader.GetInt32("ds_qtd");

                endereco.Add(DTO);
            }
            reader.Close();
            return endereco;
        }
    }
}
