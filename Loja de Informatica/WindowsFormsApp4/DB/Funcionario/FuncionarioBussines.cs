﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Funcionario
{
    class FuncionarioBussines
    {
        public int Salvar(FuncionarioDTO cliente)
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            int id = deptodb.Salvar(cliente);
            return id;
        }
        public void Alterar(FuncionarioDTO cliente)
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            deptodb.Alterar(cliente);
        }
        public void Remove(int idcliente)
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            deptodb.Remover(idcliente);
        }
        public List<FuncionarioDTO> Listar()
        {
            FuncionarioDataBase deptodb = new FuncionarioDataBase();
            List<FuncionarioDTO> depto = deptodb.Listar();
            return depto;
        }
    }
}
