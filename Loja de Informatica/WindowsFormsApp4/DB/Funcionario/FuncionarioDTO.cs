﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp4.DB.Funcionario
{
    public class FuncionarioDTO
    {
        public int  Id { get; set; }
        public string Nome { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string CPF { get; set; }
        public int IdDepartamento { get; set; }
        public int IdEndereco { get; set; }
        
    }
}
