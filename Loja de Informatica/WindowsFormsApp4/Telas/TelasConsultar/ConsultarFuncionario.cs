﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas.Telas_Consultar
{
    public partial class ConsultarFuncionario : Form
    {
        public ConsultarFuncionario()
        {
            InitializeComponent();
        }

        private void ConsultarCliente_Load(object sender, EventArgs e)
        {

        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            DB.Funcionario.FuncionarioBussines bus = new DB.Funcionario.FuncionarioBussines();
            List<DB.Funcionario.FuncionarioDTO> fun = bus.Listar();
            gvFuncionario.AutoGenerateColumns = false;
            gvFuncionario.DataSource = fun;
            
        }

        private void gvCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
