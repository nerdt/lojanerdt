﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class ConsultarFornecedor : Form
    {
        public ConsultarFornecedor()
        {
            InitializeComponent();
        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            DB.Fornecedor.FornecedorBussines bus = new DB.Fornecedor.FornecedorBussines();
            List<DB.Fornecedor.FornecedorDTO> forn = bus.Listar();
            gvFornecedor.AutoGenerateColumns = false;
            gvFornecedor.DataSource = forn;
        }
    }
}
