﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class ConsultarCliente : Form
    {
        public ConsultarCliente()
        {
            InitializeComponent();
        }

        private void ConsultarCliente_Load(object sender, EventArgs e)
        {

        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            DB.Cliente.ClienteBussines bus = new DB.Cliente.ClienteBussines();
            List<DB.Cliente.ClienteDTO> dto = bus.Listar();
            gvCliente.AutoGenerateColumns = false;
            gvCliente.DataSource = dto;

        }
    }
}
