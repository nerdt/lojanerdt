﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class frmConsultarPecas : Form
    {
        public frmConsultarPecas()
        {
            InitializeComponent();
        }

        private void Frmconsultar_Load(object sender, EventArgs e)
        {

        }

        private void btnpesquisar_Click(object sender, EventArgs e)
        {
            DB.Peças.PeçasBussines bus = new DB.Peças.PeçasBussines();
            List<DB.Peças.PeçasDTO> pecas = bus.Listar();
            gvPecas.AutoGenerateColumns = false;
            gvPecas.DataSource = pecas;
        }
    }
}
