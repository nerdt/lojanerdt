﻿namespace WindowsFormsApp4.Telas
{
    partial class frmRegistrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRegistro = new System.Windows.Forms.Label();
            this.btnRGCliente = new System.Windows.Forms.Button();
            this.btnRGFuncionario = new System.Windows.Forms.Button();
            this.btnRGFornecedor = new System.Windows.Forms.Button();
            this.btnRGdepartamento = new System.Windows.Forms.Button();
            this.btnRGPeca = new System.Windows.Forms.Button();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblRegistro
            // 
            this.lblRegistro.AutoSize = true;
            this.lblRegistro.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistro.Location = new System.Drawing.Point(12, 17);
            this.lblRegistro.Name = "lblRegistro";
            this.lblRegistro.Size = new System.Drawing.Size(92, 25);
            this.lblRegistro.TabIndex = 0;
            this.lblRegistro.Text = "Registrar";
            // 
            // btnRGCliente
            // 
            this.btnRGCliente.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGCliente.Location = new System.Drawing.Point(15, 55);
            this.btnRGCliente.Name = "btnRGCliente";
            this.btnRGCliente.Size = new System.Drawing.Size(123, 46);
            this.btnRGCliente.TabIndex = 1;
            this.btnRGCliente.Text = "Registrar Cliente";
            this.btnRGCliente.UseVisualStyleBackColor = true;
            this.btnRGCliente.Click += new System.EventHandler(this.btnRGCliente_Click);
            // 
            // btnRGFuncionario
            // 
            this.btnRGFuncionario.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGFuncionario.Location = new System.Drawing.Point(15, 112);
            this.btnRGFuncionario.Name = "btnRGFuncionario";
            this.btnRGFuncionario.Size = new System.Drawing.Size(123, 46);
            this.btnRGFuncionario.TabIndex = 2;
            this.btnRGFuncionario.Text = "Registrar Funcionario";
            this.btnRGFuncionario.UseVisualStyleBackColor = true;
            this.btnRGFuncionario.Click += new System.EventHandler(this.btnRGFuncionario_Click);
            // 
            // btnRGFornecedor
            // 
            this.btnRGFornecedor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGFornecedor.Location = new System.Drawing.Point(15, 169);
            this.btnRGFornecedor.Name = "btnRGFornecedor";
            this.btnRGFornecedor.Size = new System.Drawing.Size(123, 46);
            this.btnRGFornecedor.TabIndex = 3;
            this.btnRGFornecedor.Text = "Registrar Fornecedor";
            this.btnRGFornecedor.UseVisualStyleBackColor = true;
            this.btnRGFornecedor.Click += new System.EventHandler(this.btnRGFornecedor_Click);
            // 
            // btnRGdepartamento
            // 
            this.btnRGdepartamento.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGdepartamento.Location = new System.Drawing.Point(15, 283);
            this.btnRGdepartamento.Name = "btnRGdepartamento";
            this.btnRGdepartamento.Size = new System.Drawing.Size(123, 46);
            this.btnRGdepartamento.TabIndex = 4;
            this.btnRGdepartamento.Text = "Registrar Departamento";
            this.btnRGdepartamento.UseVisualStyleBackColor = true;
            this.btnRGdepartamento.Click += new System.EventHandler(this.btnRGdepartamento_Click);
            // 
            // btnRGPeca
            // 
            this.btnRGPeca.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRGPeca.Location = new System.Drawing.Point(15, 226);
            this.btnRGPeca.Name = "btnRGPeca";
            this.btnRGPeca.Size = new System.Drawing.Size(123, 46);
            this.btnRGPeca.TabIndex = 5;
            this.btnRGPeca.Text = "Registrar Peça";
            this.btnRGPeca.UseVisualStyleBackColor = true;
            this.btnRGPeca.Click += new System.EventHandler(this.btnRGPeca_Click);
            // 
            // btnVoltar
            // 
            this.btnVoltar.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.Location = new System.Drawing.Point(285, 283);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(157, 46);
            this.btnVoltar.TabIndex = 6;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // frmRegistrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 352);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.btnRGPeca);
            this.Controls.Add(this.btnRGdepartamento);
            this.Controls.Add(this.btnRGFornecedor);
            this.Controls.Add(this.btnRGFuncionario);
            this.Controls.Add(this.btnRGCliente);
            this.Controls.Add(this.lblRegistro);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmRegistrar";
            this.Text = "Registrar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRegistro;
        private System.Windows.Forms.Button btnRGCliente;
        private System.Windows.Forms.Button btnRGFuncionario;
        private System.Windows.Forms.Button btnRGFornecedor;
        private System.Windows.Forms.Button btnRGdepartamento;
        private System.Windows.Forms.Button btnRGPeca;
        private System.Windows.Forms.Button btnVoltar;
    }
}