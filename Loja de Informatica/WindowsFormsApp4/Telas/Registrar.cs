﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class frmRegistrar : Form
    {
        public frmRegistrar()
        {
            InitializeComponent();
        }

        private void btnRGCliente_Click(object sender, EventArgs e)
        {
            frmCadastrarCliente tela = new frmCadastrarCliente();
            tela.Show();
        }

        private void btnRGFuncionario_Click(object sender, EventArgs e)
        {
            Registrar_funcionario tela = new Registrar_funcionario();
            tela.Show();
        }

        private void btnRGFornecedor_Click(object sender, EventArgs e)
        {
            Registo_de_fornecedor tela = new Registo_de_fornecedor();
            tela.Show();
        }

        private void btnRGPeca_Click(object sender, EventArgs e)
        {
            frmRegistrarPeca tela = new frmRegistrarPeca();
            tela.Show();
        }

        private void btnRGdepartamento_Click(object sender, EventArgs e)
        {
            Telas.Telas_Registro.FrmRegistrarDepartamento tela = new Telas_Registro.FrmRegistrarDepartamento();
            tela.Show();
       
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {

        }
    }
}
