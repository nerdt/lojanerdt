﻿namespace WindowsFormsApp4.Telas.Telas_Registro
{
    partial class FrmRegistrarDepartamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmRegistrarDepartamento));
            this.txtNomedepto = new System.Windows.Forms.TextBox();
            this.lblNomeFor = new System.Windows.Forms.Label();
            this.lbllocal = new System.Windows.Forms.Label();
            this.txtlocal = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnRegistrardepto = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNomedepto
            // 
            this.txtNomedepto.Location = new System.Drawing.Point(100, 19);
            this.txtNomedepto.Name = "txtNomedepto";
            this.txtNomedepto.Size = new System.Drawing.Size(265, 20);
            this.txtNomedepto.TabIndex = 40;
            // 
            // lblNomeFor
            // 
            this.lblNomeFor.AutoSize = true;
            this.lblNomeFor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFor.Location = new System.Drawing.Point(12, 19);
            this.lblNomeFor.Name = "lblNomeFor";
            this.lblNomeFor.Size = new System.Drawing.Size(63, 25);
            this.lblNomeFor.TabIndex = 39;
            this.lblNomeFor.Text = "Nome";
            // 
            // lbllocal
            // 
            this.lbllocal.AutoSize = true;
            this.lbllocal.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllocal.Location = new System.Drawing.Point(12, 79);
            this.lbllocal.Name = "lbllocal";
            this.lbllocal.Size = new System.Drawing.Size(56, 25);
            this.lbllocal.TabIndex = 61;
            this.lbllocal.Text = "Local";
            // 
            // txtlocal
            // 
            this.txtlocal.Location = new System.Drawing.Point(100, 79);
            this.txtlocal.Multiline = true;
            this.txtlocal.Name = "txtlocal";
            this.txtlocal.Size = new System.Drawing.Size(265, 73);
            this.txtlocal.TabIndex = 60;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(482, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 146);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 62;
            this.pictureBox1.TabStop = false;
            // 
            // btnRegistrardepto
            // 
            this.btnRegistrardepto.Location = new System.Drawing.Point(100, 190);
            this.btnRegistrardepto.Name = "btnRegistrardepto";
            this.btnRegistrardepto.Size = new System.Drawing.Size(265, 66);
            this.btnRegistrardepto.TabIndex = 63;
            this.btnRegistrardepto.Text = "Registrar";
            this.btnRegistrardepto.UseVisualStyleBackColor = true;
            this.btnRegistrardepto.Click += new System.EventHandler(this.btnRegistrardepto_Click);
            // 
            // FrmRegistrarDepartamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 281);
            this.Controls.Add(this.btnRegistrardepto);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbllocal);
            this.Controls.Add(this.txtlocal);
            this.Controls.Add(this.txtNomedepto);
            this.Controls.Add(this.lblNomeFor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FrmRegistrarDepartamento";
            this.Text = "Registrar Departamento";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNomedepto;
        private System.Windows.Forms.Label lblNomeFor;
        private System.Windows.Forms.Label lbllocal;
        private System.Windows.Forms.TextBox txtlocal;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnRegistrardepto;
    }
}