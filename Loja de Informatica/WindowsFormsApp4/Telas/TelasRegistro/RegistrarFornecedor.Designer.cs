﻿namespace WindowsFormsApp4
{
    partial class Registo_de_fornecedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registo_de_fornecedor));
            this.txtNomeFor = new System.Windows.Forms.TextBox();
            this.lblNomeFor = new System.Windows.Forms.Label();
            this.txtEmailFor = new System.Windows.Forms.TextBox();
            this.lblemailFor = new System.Windows.Forms.Label();
            this.lblCNPJ = new System.Windows.Forms.Label();
            this.cboEstadoF = new System.Windows.Forms.ComboBox();
            this.txtCidadeF = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.lblCidadeF = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblTipodeproduto = new System.Windows.Forms.Label();
            this.btnRegistrarfor = new System.Windows.Forms.Button();
            this.txtCEPF = new System.Windows.Forms.MaskedTextBox();
            this.txtCPFF = new System.Windows.Forms.MaskedTextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNomeFor
            // 
            this.txtNomeFor.Location = new System.Drawing.Point(103, 12);
            this.txtNomeFor.Name = "txtNomeFor";
            this.txtNomeFor.Size = new System.Drawing.Size(241, 20);
            this.txtNomeFor.TabIndex = 38;
            // 
            // lblNomeFor
            // 
            this.lblNomeFor.AutoSize = true;
            this.lblNomeFor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeFor.Location = new System.Drawing.Point(12, 12);
            this.lblNomeFor.Name = "lblNomeFor";
            this.lblNomeFor.Size = new System.Drawing.Size(63, 25);
            this.lblNomeFor.TabIndex = 37;
            this.lblNomeFor.Text = "Nome";
            // 
            // txtEmailFor
            // 
            this.txtEmailFor.Location = new System.Drawing.Point(103, 53);
            this.txtEmailFor.Name = "txtEmailFor";
            this.txtEmailFor.Size = new System.Drawing.Size(241, 20);
            this.txtEmailFor.TabIndex = 60;
            // 
            // lblemailFor
            // 
            this.lblemailFor.AutoSize = true;
            this.lblemailFor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemailFor.Location = new System.Drawing.Point(12, 48);
            this.lblemailFor.Name = "lblemailFor";
            this.lblemailFor.Size = new System.Drawing.Size(66, 25);
            this.lblemailFor.TabIndex = 59;
            this.lblemailFor.Text = "E-mail";
            // 
            // lblCNPJ
            // 
            this.lblCNPJ.AutoSize = true;
            this.lblCNPJ.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCNPJ.Location = new System.Drawing.Point(12, 92);
            this.lblCNPJ.Name = "lblCNPJ";
            this.lblCNPJ.Size = new System.Drawing.Size(56, 25);
            this.lblCNPJ.TabIndex = 54;
            this.lblCNPJ.Text = "CNPJ";
            // 
            // cboEstadoF
            // 
            this.cboEstadoF.FormattingEnabled = true;
            this.cboEstadoF.Items.AddRange(new object[] {
            "Acre",
            "Alagoas",
            "Amapá",
            "Amazonas",
            "Bahia",
            "Ceará",
            "Distrito Federal",
            "Espírito Santo",
            "Goiás",
            "Maranhão",
            "Mato Grosso",
            "Mato Grosso do Sul",
            "Minas Gerais",
            "Pará",
            "Paraíba",
            "Paraná",
            "Pernambuco",
            "Piauí",
            "Rio de Janeiro",
            "Rio Grande do Norte",
            "Rio Grande do Sul",
            "Rondônia",
            "Roraima",
            "Santa Catarina",
            "São Paulo",
            "Sergipe",
            "Tocantins"});
            this.cboEstadoF.Location = new System.Drawing.Point(86, 254);
            this.cboEstadoF.Name = "cboEstadoF";
            this.cboEstadoF.Size = new System.Drawing.Size(100, 21);
            this.cboEstadoF.TabIndex = 68;
            // 
            // txtCidadeF
            // 
            this.txtCidadeF.Location = new System.Drawing.Point(86, 295);
            this.txtCidadeF.Name = "txtCidadeF";
            this.txtCidadeF.Size = new System.Drawing.Size(100, 20);
            this.txtCidadeF.TabIndex = 65;
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(12, 346);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(45, 25);
            this.lblCEP.TabIndex = 63;
            this.lblCEP.Text = "CEP";
            // 
            // lblCidadeF
            // 
            this.lblCidadeF.AutoSize = true;
            this.lblCidadeF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidadeF.Location = new System.Drawing.Point(12, 290);
            this.lblCidadeF.Name = "lblCidadeF";
            this.lblCidadeF.Size = new System.Drawing.Size(71, 25);
            this.lblCidadeF.TabIndex = 62;
            this.lblCidadeF.Text = "Cidade";
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(12, 248);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(68, 25);
            this.lblestado.TabIndex = 61;
            this.lblestado.Text = "Estado";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(381, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            // 
            // lblTipodeproduto
            // 
            this.lblTipodeproduto.AutoSize = true;
            this.lblTipodeproduto.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipodeproduto.Location = new System.Drawing.Point(12, 135);
            this.lblTipodeproduto.Name = "lblTipodeproduto";
            this.lblTipodeproduto.Size = new System.Drawing.Size(132, 25);
            this.lblTipodeproduto.TabIndex = 87;
            this.lblTipodeproduto.Text = "Fornecimento:";
            // 
            // btnRegistrarfor
            // 
            this.btnRegistrarfor.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarfor.Location = new System.Drawing.Point(206, 424);
            this.btnRegistrarfor.Name = "btnRegistrarfor";
            this.btnRegistrarfor.Size = new System.Drawing.Size(157, 56);
            this.btnRegistrarfor.TabIndex = 88;
            this.btnRegistrarfor.Text = "Registrar";
            this.btnRegistrarfor.UseVisualStyleBackColor = true;
            this.btnRegistrarfor.Click += new System.EventHandler(this.btnRegistrarfor_Click);
            // 
            // txtCEPF
            // 
            this.txtCEPF.Location = new System.Drawing.Point(86, 351);
            this.txtCEPF.Mask = "000.000.000-00";
            this.txtCEPF.Name = "txtCEPF";
            this.txtCEPF.Size = new System.Drawing.Size(91, 20);
            this.txtCEPF.TabIndex = 94;
            this.txtCEPF.MaskInputRejected += new System.Windows.Forms.MaskInputRejectedEventHandler(this.txtCEP_MaskInputRejected);
            // 
            // txtCPFF
            // 
            this.txtCPFF.Location = new System.Drawing.Point(103, 97);
            this.txtCPFF.Mask = "000.000.000-00";
            this.txtCPFF.Name = "txtCPFF";
            this.txtCPFF.Size = new System.Drawing.Size(94, 20);
            this.txtCPFF.TabIndex = 93;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(16, 163);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(241, 20);
            this.textBox1.TabIndex = 95;
            // 
            // Registo_de_fornecedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 492);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.txtCEPF);
            this.Controls.Add(this.txtCPFF);
            this.Controls.Add(this.btnRegistrarfor);
            this.Controls.Add(this.lblTipodeproduto);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.cboEstadoF);
            this.Controls.Add(this.txtCidadeF);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.lblCidadeF);
            this.Controls.Add(this.lblestado);
            this.Controls.Add(this.txtEmailFor);
            this.Controls.Add(this.lblemailFor);
            this.Controls.Add(this.lblCNPJ);
            this.Controls.Add(this.txtNomeFor);
            this.Controls.Add(this.lblNomeFor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Registo_de_fornecedor";
            this.Text = "Registo de Fornecedor";
            this.Load += new System.EventHandler(this.Registo_de_fornecedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNomeFor;
        private System.Windows.Forms.Label lblNomeFor;
        private System.Windows.Forms.TextBox txtEmailFor;
        private System.Windows.Forms.Label lblemailFor;
        private System.Windows.Forms.Label lblCNPJ;
        private System.Windows.Forms.ComboBox cboEstadoF;
        private System.Windows.Forms.TextBox txtCidadeF;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Label lblCidadeF;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblTipodeproduto;
        private System.Windows.Forms.Button btnRegistrarfor;
        private System.Windows.Forms.MaskedTextBox txtCEPF;
        private System.Windows.Forms.MaskedTextBox txtCPFF;
        private System.Windows.Forms.TextBox textBox1;
    }
}