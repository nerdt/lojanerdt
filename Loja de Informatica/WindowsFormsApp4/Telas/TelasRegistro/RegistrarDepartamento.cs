﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas.Telas_Registro
{
    public partial class FrmRegistrarDepartamento : Form
    {
        public FrmRegistrarDepartamento()
        {
            InitializeComponent();
        }

        private void btnRegistrardepto_Click(object sender, EventArgs e)
        {
            DB.Departamento.DeptoDTO dto = new DB.Departamento.DeptoDTO();
            dto.Nome = txtNomedepto.Text;
            dto.Local = txtlocal.Text;

            DB.Departamento.DeptoBussines business = new DB.Departamento.DeptoBussines();
            business.Salvar(dto);

            MessageBox.Show("Departamento registrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
