﻿namespace WindowsFormsApp4
{
    partial class frmRegistrarPeca
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistrarPeca));
            this.lblNomeP = new System.Windows.Forms.Label();
            this.lblQuantidadeP = new System.Windows.Forms.Label();
            this.lblPreco = new System.Windows.Forms.Label();
            this.txtnomeP = new System.Windows.Forms.TextBox();
            this.txtpreco = new System.Windows.Forms.TextBox();
            this.txtdescricao = new System.Windows.Forms.TextBox();
            this.lbldescricao = new System.Windows.Forms.Label();
            this.txtquantidadeP = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.cboTipoP = new System.Windows.Forms.ComboBox();
            this.lblTipoP = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNomeP
            // 
            this.lblNomeP.AutoSize = true;
            this.lblNomeP.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeP.Location = new System.Drawing.Point(12, 22);
            this.lblNomeP.Name = "lblNomeP";
            this.lblNomeP.Size = new System.Drawing.Size(134, 25);
            this.lblNomeP.TabIndex = 0;
            this.lblNomeP.Text = "Nome da peça";
            this.lblNomeP.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblQuantidadeP
            // 
            this.lblQuantidadeP.AutoSize = true;
            this.lblQuantidadeP.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuantidadeP.Location = new System.Drawing.Point(12, 75);
            this.lblQuantidadeP.Name = "lblQuantidadeP";
            this.lblQuantidadeP.Size = new System.Drawing.Size(111, 25);
            this.lblQuantidadeP.TabIndex = 1;
            this.lblQuantidadeP.Text = "Quantidade";
            this.lblQuantidadeP.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblPreco
            // 
            this.lblPreco.AutoSize = true;
            this.lblPreco.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPreco.Location = new System.Drawing.Point(152, 75);
            this.lblPreco.Name = "lblPreco";
            this.lblPreco.Size = new System.Drawing.Size(57, 25);
            this.lblPreco.TabIndex = 2;
            this.lblPreco.Text = "Valor";
            this.lblPreco.Click += new System.EventHandler(this.label3_Click);
            // 
            // txtnomeP
            // 
            this.txtnomeP.Location = new System.Drawing.Point(12, 49);
            this.txtnomeP.Name = "txtnomeP";
            this.txtnomeP.Size = new System.Drawing.Size(251, 20);
            this.txtnomeP.TabIndex = 3;
            // 
            // txtpreco
            // 
            this.txtpreco.Location = new System.Drawing.Point(136, 101);
            this.txtpreco.Name = "txtpreco";
            this.txtpreco.Size = new System.Drawing.Size(127, 20);
            this.txtpreco.TabIndex = 5;
            // 
            // txtdescricao
            // 
            this.txtdescricao.Location = new System.Drawing.Point(11, 160);
            this.txtdescricao.Name = "txtdescricao";
            this.txtdescricao.Size = new System.Drawing.Size(252, 20);
            this.txtdescricao.TabIndex = 6;
            // 
            // lbldescricao
            // 
            this.lbldescricao.AutoSize = true;
            this.lbldescricao.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldescricao.Location = new System.Drawing.Point(12, 134);
            this.lbldescricao.Name = "lbldescricao";
            this.lbldescricao.Size = new System.Drawing.Size(94, 25);
            this.lbldescricao.TabIndex = 7;
            this.lbldescricao.Text = "Descrição";
            // 
            // txtquantidadeP
            // 
            this.txtquantidadeP.Location = new System.Drawing.Point(12, 101);
            this.txtquantidadeP.Name = "txtquantidadeP";
            this.txtquantidadeP.Size = new System.Drawing.Size(118, 20);
            this.txtquantidadeP.TabIndex = 8;
            this.txtquantidadeP.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // btnEnviar
            // 
            this.btnEnviar.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.Location = new System.Drawing.Point(342, 172);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(119, 58);
            this.btnEnviar.TabIndex = 9;
            this.btnEnviar.TabStop = false;
            this.btnEnviar.Text = "Registrar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.ImageLocation = "";
            this.pictureBox1.Location = new System.Drawing.Point(342, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(148, 142);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // cboTipoP
            // 
            this.cboTipoP.FormattingEnabled = true;
            this.cboTipoP.Items.AddRange(new object[] {
            "Placa-mãe",
            "Processador",
            "Memoria RAM",
            "HD",
            "Fonte",
            "Placa de rede",
            "Placa de video "});
            this.cboTipoP.Location = new System.Drawing.Point(11, 209);
            this.cboTipoP.Name = "cboTipoP";
            this.cboTipoP.Size = new System.Drawing.Size(252, 21);
            this.cboTipoP.TabIndex = 11;
            this.cboTipoP.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblTipoP
            // 
            this.lblTipoP.AutoSize = true;
            this.lblTipoP.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoP.Location = new System.Drawing.Point(12, 183);
            this.lblTipoP.Name = "lblTipoP";
            this.lblTipoP.Size = new System.Drawing.Size(49, 25);
            this.lblTipoP.TabIndex = 12;
            this.lblTipoP.Text = "Tipo";
            // 
            // frmRegistrarPeca
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 266);
            this.Controls.Add(this.lblTipoP);
            this.Controls.Add(this.cboTipoP);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.txtquantidadeP);
            this.Controls.Add(this.lbldescricao);
            this.Controls.Add(this.txtdescricao);
            this.Controls.Add(this.txtpreco);
            this.Controls.Add(this.txtnomeP);
            this.Controls.Add(this.lblPreco);
            this.Controls.Add(this.lblQuantidadeP);
            this.Controls.Add(this.lblNomeP);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmRegistrarPeca";
            this.Text = "Registrar Peças";
            this.TransparencyKey = System.Drawing.Color.Green;
            this.Load += new System.EventHandler(this.FrmRGpecas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNomeP;
        private System.Windows.Forms.Label lblQuantidadeP;
        private System.Windows.Forms.Label lblPreco;
        private System.Windows.Forms.TextBox txtnomeP;
        private System.Windows.Forms.TextBox txtpreco;
        private System.Windows.Forms.TextBox txtdescricao;
        private System.Windows.Forms.Label lbldescricao;
        private System.Windows.Forms.TextBox txtquantidadeP;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cboTipoP;
        private System.Windows.Forms.Label lblTipoP;
    }
}

