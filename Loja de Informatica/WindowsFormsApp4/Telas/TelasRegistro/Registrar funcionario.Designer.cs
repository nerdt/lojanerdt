﻿namespace WindowsFormsApp4
{
    partial class Registrar_funcionario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registrar_funcionario));
            this.cboEstadoC = new System.Windows.Forms.ComboBox();
            this.txtEmailF = new System.Windows.Forms.TextBox();
            this.lblemailF = new System.Windows.Forms.Label();
            this.lblNumero2F = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.lblCEP = new System.Windows.Forms.Label();
            this.lblCidade = new System.Windows.Forms.Label();
            this.lblestado = new System.Windows.Forms.Label();
            this.txtNomeF = new System.Windows.Forms.TextBox();
            this.lblCPFF = new System.Windows.Forms.Label();
            this.lblTelefone1F = new System.Windows.Forms.Label();
            this.lblNomeF = new System.Windows.Forms.Label();
            this.lblRGF = new System.Windows.Forms.Label();
            this.cboDepartamento = new System.Windows.Forms.ComboBox();
            this.lblDepartamento = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnRegistrarCliente = new System.Windows.Forms.Button();
            this.txtFixo = new System.Windows.Forms.MaskedTextBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtCEP = new System.Windows.Forms.MaskedTextBox();
            this.txtCelular = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // cboEstadoC
            // 
            this.cboEstadoC.FormattingEnabled = true;
            this.cboEstadoC.Items.AddRange(new object[] {
            "AC",
            "AL ",
            "AP ",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP"});
            this.cboEstadoC.Location = new System.Drawing.Point(108, 290);
            this.cboEstadoC.Name = "cboEstadoC";
            this.cboEstadoC.Size = new System.Drawing.Size(61, 21);
            this.cboEstadoC.TabIndex = 53;
            this.cboEstadoC.SelectedIndexChanged += new System.EventHandler(this.cboEstadoC_SelectedIndexChanged);
            // 
            // txtEmailF
            // 
            this.txtEmailF.Location = new System.Drawing.Point(139, 87);
            this.txtEmailF.Name = "txtEmailF";
            this.txtEmailF.Size = new System.Drawing.Size(259, 20);
            this.txtEmailF.TabIndex = 52;
            // 
            // lblemailF
            // 
            this.lblemailF.AutoSize = true;
            this.lblemailF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemailF.Location = new System.Drawing.Point(9, 87);
            this.lblemailF.Name = "lblemailF";
            this.lblemailF.Size = new System.Drawing.Size(66, 25);
            this.lblemailF.TabIndex = 51;
            this.lblemailF.Text = "E-mail";
            // 
            // lblNumero2F
            // 
            this.lblNumero2F.AutoSize = true;
            this.lblNumero2F.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero2F.Location = new System.Drawing.Point(9, 193);
            this.lblNumero2F.Name = "lblNumero2F";
            this.lblNumero2F.Size = new System.Drawing.Size(72, 25);
            this.lblNumero2F.TabIndex = 46;
            this.lblNumero2F.Text = "Celular";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(341, 293);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(172, 20);
            this.txtCidade.TabIndex = 43;
            this.txtCidade.TextChanged += new System.EventHandler(this.txtCidade_TextChanged);
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.Location = new System.Drawing.Point(28, 335);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(45, 25);
            this.lblCEP.TabIndex = 41;
            this.lblCEP.Text = "CEP";
            this.lblCEP.Click += new System.EventHandler(this.lblCEP_Click);
            // 
            // lblCidade
            // 
            this.lblCidade.AutoSize = true;
            this.lblCidade.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCidade.Location = new System.Drawing.Point(198, 288);
            this.lblCidade.Name = "lblCidade";
            this.lblCidade.Size = new System.Drawing.Size(71, 25);
            this.lblCidade.TabIndex = 40;
            this.lblCidade.Text = "Cidade";
            this.lblCidade.Click += new System.EventHandler(this.lblCidade_Click);
            // 
            // lblestado
            // 
            this.lblestado.AutoSize = true;
            this.lblestado.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblestado.Location = new System.Drawing.Point(28, 285);
            this.lblestado.Name = "lblestado";
            this.lblestado.Size = new System.Drawing.Size(68, 25);
            this.lblestado.TabIndex = 39;
            this.lblestado.Text = "Estado";
            this.lblestado.Click += new System.EventHandler(this.lblestado_Click);
            // 
            // txtNomeF
            // 
            this.txtNomeF.Location = new System.Drawing.Point(139, 34);
            this.txtNomeF.Name = "txtNomeF";
            this.txtNomeF.Size = new System.Drawing.Size(259, 20);
            this.txtNomeF.TabIndex = 36;
            this.txtNomeF.TextChanged += new System.EventHandler(this.txtNomeF_TextChanged);
            // 
            // lblCPFF
            // 
            this.lblCPFF.AutoSize = true;
            this.lblCPFF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPFF.Location = new System.Drawing.Point(12, 147);
            this.lblCPFF.Name = "lblCPFF";
            this.lblCPFF.Size = new System.Drawing.Size(44, 25);
            this.lblCPFF.TabIndex = 35;
            this.lblCPFF.Text = "CPF";
            // 
            // lblTelefone1F
            // 
            this.lblTelefone1F.AutoSize = true;
            this.lblTelefone1F.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone1F.Location = new System.Drawing.Point(255, 199);
            this.lblTelefone1F.Name = "lblTelefone1F";
            this.lblTelefone1F.Size = new System.Drawing.Size(46, 25);
            this.lblTelefone1F.TabIndex = 34;
            this.lblTelefone1F.Text = "Fixo";
            // 
            // lblNomeF
            // 
            this.lblNomeF.AutoSize = true;
            this.lblNomeF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeF.Location = new System.Drawing.Point(12, 34);
            this.lblNomeF.Name = "lblNomeF";
            this.lblNomeF.Size = new System.Drawing.Size(63, 25);
            this.lblNomeF.TabIndex = 33;
            this.lblNomeF.Text = "Nome";
            this.lblNomeF.Click += new System.EventHandler(this.lblNomeF_Click);
            // 
            // lblRGF
            // 
            this.lblRGF.AutoSize = true;
            this.lblRGF.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRGF.Location = new System.Drawing.Point(255, 153);
            this.lblRGF.Name = "lblRGF";
            this.lblRGF.Size = new System.Drawing.Size(36, 25);
            this.lblRGF.TabIndex = 54;
            this.lblRGF.Text = "RG";
            // 
            // cboDepartamento
            // 
            this.cboDepartamento.FormattingEnabled = true;
            this.cboDepartamento.Items.AddRange(new object[] {
            "Tecnico",
            "Atendente",
            "Caixa",
            "Gerente"});
            this.cboDepartamento.Location = new System.Drawing.Point(341, 332);
            this.cboDepartamento.Name = "cboDepartamento";
            this.cboDepartamento.Size = new System.Drawing.Size(172, 21);
            this.cboDepartamento.TabIndex = 56;
            // 
            // lblDepartamento
            // 
            this.lblDepartamento.AutoSize = true;
            this.lblDepartamento.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDepartamento.Location = new System.Drawing.Point(198, 332);
            this.lblDepartamento.Name = "lblDepartamento";
            this.lblDepartamento.Size = new System.Drawing.Size(133, 25);
            this.lblDepartamento.TabIndex = 57;
            this.lblDepartamento.Text = "Departamento";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(434, 30);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 58;
            this.pictureBox1.TabStop = false;
            // 
            // btnRegistrarCliente
            // 
            this.btnRegistrarCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRegistrarCliente.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarCliente.Location = new System.Drawing.Point(215, 391);
            this.btnRegistrarCliente.Name = "btnRegistrarCliente";
            this.btnRegistrarCliente.Size = new System.Drawing.Size(183, 64);
            this.btnRegistrarCliente.TabIndex = 59;
            this.btnRegistrarCliente.Text = "Registrar";
            this.btnRegistrarCliente.UseVisualStyleBackColor = true;
            this.btnRegistrarCliente.Click += new System.EventHandler(this.btnRegistrarCliente_Click);
            // 
            // txtFixo
            // 
            this.txtFixo.Location = new System.Drawing.Point(307, 205);
            this.txtFixo.Mask = "(99) 00000-0000";
            this.txtFixo.Name = "txtFixo";
            this.txtFixo.Size = new System.Drawing.Size(91, 20);
            this.txtFixo.TabIndex = 61;
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(139, 153);
            this.txtCPF.Mask = "000.000.000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(91, 20);
            this.txtCPF.TabIndex = 62;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(307, 159);
            this.txtRG.Mask = "00.000.000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(91, 20);
            this.txtRG.TabIndex = 63;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.pictureBox2.Location = new System.Drawing.Point(0, 247);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(703, 10);
            this.pictureBox2.TabIndex = 64;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // txtCEP
            // 
            this.txtCEP.Location = new System.Drawing.Point(108, 335);
            this.txtCEP.Mask = "00000-000";
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.Size = new System.Drawing.Size(61, 20);
            this.txtCEP.TabIndex = 65;
            // 
            // txtCelular
            // 
            this.txtCelular.Location = new System.Drawing.Point(139, 199);
            this.txtCelular.Mask = "(99) 00000-0000";
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(91, 20);
            this.txtCelular.TabIndex = 60;
            // 
            // Registrar_funcionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 479);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.txtRG);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.txtFixo);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.btnRegistrarCliente);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblDepartamento);
            this.Controls.Add(this.cboDepartamento);
            this.Controls.Add(this.lblRGF);
            this.Controls.Add(this.cboEstadoC);
            this.Controls.Add(this.txtEmailF);
            this.Controls.Add(this.lblemailF);
            this.Controls.Add(this.lblNumero2F);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.lblCidade);
            this.Controls.Add(this.lblestado);
            this.Controls.Add(this.txtNomeF);
            this.Controls.Add(this.lblCPFF);
            this.Controls.Add(this.lblTelefone1F);
            this.Controls.Add(this.lblNomeF);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Registrar_funcionario";
            this.Text = "Registrar Funcionario";
            this.Load += new System.EventHandler(this.Registrar_funcionario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboEstadoC;
        private System.Windows.Forms.TextBox txtEmailF;
        private System.Windows.Forms.Label lblemailF;
        private System.Windows.Forms.Label lblNumero2F;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Label lblCidade;
        private System.Windows.Forms.Label lblestado;
        private System.Windows.Forms.TextBox txtNomeF;
        private System.Windows.Forms.Label lblCPFF;
        private System.Windows.Forms.Label lblTelefone1F;
        private System.Windows.Forms.Label lblNomeF;
        private System.Windows.Forms.Label lblRGF;
        private System.Windows.Forms.ComboBox cboDepartamento;
        private System.Windows.Forms.Label lblDepartamento;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnRegistrarCliente;
        private System.Windows.Forms.MaskedTextBox txtFixo;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MaskedTextBox txtCEP;
        private System.Windows.Forms.MaskedTextBox txtCelular;
    }
}