﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class frmCadastrarCliente : Form
    {
        public frmCadastrarCliente()
        {
            InitializeComponent();
        }

        private void frmCadastrarCliente_Load(object sender, EventArgs e)
        {

        }

        private void txtCEP_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnRegistrarCliente_Click(object sender, EventArgs e)
        {
            DB.Endereço.EndereçoDTO endereco = cboEstadoC.SelectedItem as DB.Endereço.EndereçoDTO;

            DB.Cliente.ClienteDTO dto = new DB.Cliente.ClienteDTO();
            dto.Nome = txtNome.Text;
            dto.Telefone = txtcelular.Text;
            dto.CPF = txtCEP.Text;
            dto.idEndereco = endereco.Id;
            dto.Email = txtemail.Text;
            dto.Celular = txtcelular.Text;
            DB.Cliente.ClienteBussines bus = new DB.Cliente.ClienteBussines();
            bus.Salvar(dto);
            MessageBox.Show("Cliente registrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
        }
    }
}
