﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Registrar_funcionario : Form
    {
        public Registrar_funcionario()
        {
            InitializeComponent();
        }

        private void lblCidade_Click(object sender, EventArgs e)
        {

        }

        private void txtRua_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblRua_Click(object sender, EventArgs e)
        {

        }

        private void txtCidade_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCEP_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblCEP_Click(object sender, EventArgs e)
        {

        }

        private void cboEstadoC_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblestado_Click(object sender, EventArgs e)
        {

        }

        private void txtSobrenomeF_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblSobrenomeF_Click(object sender, EventArgs e)
        {

        }

        private void txtNomeF_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblNomeF_Click(object sender, EventArgs e)
        {

        }

        private void txtNumero1F_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void Registrar_funcionario_Load(object sender, EventArgs e)
        {

        }

        private void btnRegistrarCliente_Click(object sender, EventArgs e)
        {
            DB.Departamento.DeptoDTO depto = cboDepartamento.SelectedItem as DB.Departamento.DeptoDTO;
            DB.Endereço.EndereçoDTO endereco = cboEstadoC.SelectedItem as DB.Endereço.EndereçoDTO;

            DB.Funcionario.FuncionarioDTO dto = new DB.Funcionario.FuncionarioDTO();
            dto.Nome = txtNomeF.Text;
            dto.Email = txtEmailF.Text;
            dto.CPF = txtCPF.Text;
            dto.IdDepartamento = int.Parse(cboDepartamento.Text);
            dto.IdEndereco = int.Parse(cboEstadoC.Text);
            DB.Funcionario.FuncionarioBussines bus = new DB.Funcionario.FuncionarioBussines();
            bus.Salvar(dto);
            MessageBox.Show("Funcionario cadastrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
