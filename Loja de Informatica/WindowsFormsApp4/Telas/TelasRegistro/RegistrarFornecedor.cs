﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Registo_de_fornecedor : Form
    {
        public Registo_de_fornecedor()
        {
            InitializeComponent();
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Registo_de_fornecedor_Load(object sender, EventArgs e)
        {

        }

        private void txtCEP_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnRegistrarfor_Click(object sender, EventArgs e)
        {
            DB.Endereço.EndereçoDTO endereco = cboEstadoF.SelectedItem as DB.Endereço.EndereçoDTO;

            DB.Fornecedor.FornecedorDTO dto = new DB.Fornecedor.FornecedorDTO();
            dto.Nome = txtNomeFor.Text;
            dto.CNPJ = txtCPFF.Text;
            dto.IdEndereco = int.Parse(cboEstadoF.Text);
            DB.Fornecedor.FornecedorBussines bus = new DB.Fornecedor.FornecedorBussines();
            bus.Salvar(dto);
            MessageBox.Show("Fornecedor registrado com sucesso.", "NerdT", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
