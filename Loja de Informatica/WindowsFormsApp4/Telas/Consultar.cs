﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
        }

        private void btnCSCliente_Click(object sender, EventArgs e)
        {
            ConsultarCliente tela = new ConsultarCliente();
            tela.Show();
        }

        private void btnCSFuncionario_Click(object sender, EventArgs e)
        {
            Telas.Telas_Consultar.ConsultarFuncionario telas = new Telas.Telas_Consultar.ConsultarFuncionario();
            telas.Show();
        }

        private void btnCSdepartamento_Click(object sender, EventArgs e)
        {
            Telas.Telas_Consultar.ConsultarDepartamento tela = new Telas.Telas_Consultar.ConsultarDepartamento();
            tela.Show();
        }

        private void btnCSFornecedor_Click(object sender, EventArgs e)
        {
            ConsultarFornecedor tela = new ConsultarFornecedor();
            tela.Show();
        }

        private void btnCSPeca_Click(object sender, EventArgs e)
        {
            frmConsultarPecas tela = new frmConsultarPecas();
            tela.Show();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
