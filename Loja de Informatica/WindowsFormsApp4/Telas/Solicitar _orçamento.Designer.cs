﻿namespace WindowsFormsApp4
{
    partial class frmSolicitarorçamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSolicitarorçamento));
            this.txtNomeO = new System.Windows.Forms.TextBox();
            this.lblNomeO = new System.Windows.Forms.Label();
            this.txtEmailO = new System.Windows.Forms.TextBox();
            this.lblemailO = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSolicitar = new System.Windows.Forms.Button();
            this.txtcelular = new System.Windows.Forms.MaskedTextBox();
            this.txttelefone = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblTelefone1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.txtvalor = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNomeO
            // 
            this.txtNomeO.Location = new System.Drawing.Point(108, 26);
            this.txtNomeO.Name = "txtNomeO";
            this.txtNomeO.Size = new System.Drawing.Size(223, 20);
            this.txtNomeO.TabIndex = 67;
            // 
            // lblNomeO
            // 
            this.lblNomeO.AutoSize = true;
            this.lblNomeO.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeO.Location = new System.Drawing.Point(19, 20);
            this.lblNomeO.Name = "lblNomeO";
            this.lblNomeO.Size = new System.Drawing.Size(63, 25);
            this.lblNomeO.TabIndex = 66;
            this.lblNomeO.Text = "Nome";
            // 
            // txtEmailO
            // 
            this.txtEmailO.Location = new System.Drawing.Point(108, 77);
            this.txtEmailO.Name = "txtEmailO";
            this.txtEmailO.Size = new System.Drawing.Size(223, 20);
            this.txtEmailO.TabIndex = 71;
            this.txtEmailO.TextChanged += new System.EventHandler(this.txtEmailO_TextChanged);
            // 
            // lblemailO
            // 
            this.lblemailO.AutoSize = true;
            this.lblemailO.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemailO.Location = new System.Drawing.Point(19, 72);
            this.lblemailO.Name = "lblemailO";
            this.lblemailO.Size = new System.Drawing.Size(66, 25);
            this.lblemailO.TabIndex = 70;
            this.lblemailO.Text = "E-mail";
            this.lblemailO.Click += new System.EventHandler(this.lblemailO_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(457, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 76;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // btnSolicitar
            // 
            this.btnSolicitar.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSolicitar.Location = new System.Drawing.Point(457, 252);
            this.btnSolicitar.Name = "btnSolicitar";
            this.btnSolicitar.Size = new System.Drawing.Size(157, 35);
            this.btnSolicitar.TabIndex = 77;
            this.btnSolicitar.Text = "Solicitar";
            this.btnSolicitar.UseVisualStyleBackColor = true;
            this.btnSolicitar.Click += new System.EventHandler(this.btnSolicitar_Click);
            // 
            // txtcelular
            // 
            this.txtcelular.Location = new System.Drawing.Point(252, 123);
            this.txtcelular.Mask = "(00)00000-0000";
            this.txtcelular.Name = "txtcelular";
            this.txtcelular.Size = new System.Drawing.Size(79, 20);
            this.txtcelular.TabIndex = 81;
            // 
            // txttelefone
            // 
            this.txttelefone.Location = new System.Drawing.Point(89, 123);
            this.txttelefone.Mask = "(00)0000-0000";
            this.txttelefone.Name = "txttelefone";
            this.txttelefone.Size = new System.Drawing.Size(79, 20);
            this.txttelefone.TabIndex = 80;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(174, 121);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 25);
            this.label1.TabIndex = 79;
            this.label1.Text = "Celular";
            // 
            // lblTelefone1
            // 
            this.lblTelefone1.AutoSize = true;
            this.lblTelefone1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefone1.Location = new System.Drawing.Point(19, 120);
            this.lblTelefone1.Name = "lblTelefone1";
            this.lblTelefone1.Size = new System.Drawing.Size(46, 25);
            this.lblTelefone1.TabIndex = 78;
            this.lblTelefone1.Text = "Fixo";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(19, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 25);
            this.label2.TabIndex = 88;
            this.label2.Text = "Valor ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(-13, 183);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(638, 10);
            this.pictureBox2.TabIndex = 90;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // txtvalor
            // 
            this.txtvalor.Location = new System.Drawing.Point(108, 218);
            this.txtvalor.Name = "txtvalor";
            this.txtvalor.Size = new System.Drawing.Size(108, 20);
            this.txtvalor.TabIndex = 91;
            // 
            // frmSolicitarorçamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 314);
            this.Controls.Add(this.txtvalor);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtcelular);
            this.Controls.Add(this.txttelefone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTelefone1);
            this.Controls.Add(this.btnSolicitar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtEmailO);
            this.Controls.Add(this.lblemailO);
            this.Controls.Add(this.txtNomeO);
            this.Controls.Add(this.lblNomeO);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSolicitarorçamento";
            this.Text = "Solicitar orçamento";
            this.Load += new System.EventHandler(this.frmSolicitarorçamento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtNomeO;
        private System.Windows.Forms.Label lblNomeO;
        private System.Windows.Forms.TextBox txtEmailO;
        private System.Windows.Forms.Label lblemailO;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSolicitar;
        private System.Windows.Forms.MaskedTextBox txtcelular;
        private System.Windows.Forms.MaskedTextBox txttelefone;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTelefone1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtvalor;
    }
}