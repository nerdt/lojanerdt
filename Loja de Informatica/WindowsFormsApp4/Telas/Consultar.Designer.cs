﻿namespace WindowsFormsApp4.Telas
{
    partial class frmConsultar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsultar));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.btnCSPeca = new System.Windows.Forms.Button();
            this.btnCSdepartamento = new System.Windows.Forms.Button();
            this.btnCSFornecedor = new System.Windows.Forms.Button();
            this.btnCSFuncionario = new System.Windows.Forms.Button();
            this.btnCSCliente = new System.Windows.Forms.Button();
            this.lblConsultar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(287, 59);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(157, 146);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 71;
            this.pictureBox1.TabStop = false;
            // 
            // btnVoltar
            // 
            this.btnVoltar.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.Location = new System.Drawing.Point(287, 287);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(157, 46);
            this.btnVoltar.TabIndex = 70;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = true;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // btnCSPeca
            // 
            this.btnCSPeca.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCSPeca.Location = new System.Drawing.Point(17, 230);
            this.btnCSPeca.Name = "btnCSPeca";
            this.btnCSPeca.Size = new System.Drawing.Size(123, 46);
            this.btnCSPeca.TabIndex = 69;
            this.btnCSPeca.Text = "Consultar Peças";
            this.btnCSPeca.UseVisualStyleBackColor = true;
            this.btnCSPeca.Click += new System.EventHandler(this.btnCSPeca_Click);
            // 
            // btnCSdepartamento
            // 
            this.btnCSdepartamento.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCSdepartamento.Location = new System.Drawing.Point(17, 287);
            this.btnCSdepartamento.Name = "btnCSdepartamento";
            this.btnCSdepartamento.Size = new System.Drawing.Size(123, 46);
            this.btnCSdepartamento.TabIndex = 68;
            this.btnCSdepartamento.Text = "Consultar Departamentos";
            this.btnCSdepartamento.UseVisualStyleBackColor = true;
            this.btnCSdepartamento.Click += new System.EventHandler(this.btnCSdepartamento_Click);
            // 
            // btnCSFornecedor
            // 
            this.btnCSFornecedor.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCSFornecedor.Location = new System.Drawing.Point(17, 173);
            this.btnCSFornecedor.Name = "btnCSFornecedor";
            this.btnCSFornecedor.Size = new System.Drawing.Size(123, 46);
            this.btnCSFornecedor.TabIndex = 67;
            this.btnCSFornecedor.Text = "Consultar Fornecedores";
            this.btnCSFornecedor.UseVisualStyleBackColor = true;
            this.btnCSFornecedor.Click += new System.EventHandler(this.btnCSFornecedor_Click);
            // 
            // btnCSFuncionario
            // 
            this.btnCSFuncionario.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCSFuncionario.Location = new System.Drawing.Point(17, 116);
            this.btnCSFuncionario.Name = "btnCSFuncionario";
            this.btnCSFuncionario.Size = new System.Drawing.Size(123, 46);
            this.btnCSFuncionario.TabIndex = 66;
            this.btnCSFuncionario.Text = "Consultar Funcionarios";
            this.btnCSFuncionario.UseVisualStyleBackColor = true;
            this.btnCSFuncionario.Click += new System.EventHandler(this.btnCSFuncionario_Click);
            // 
            // btnCSCliente
            // 
            this.btnCSCliente.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCSCliente.Location = new System.Drawing.Point(17, 59);
            this.btnCSCliente.Name = "btnCSCliente";
            this.btnCSCliente.Size = new System.Drawing.Size(123, 46);
            this.btnCSCliente.TabIndex = 65;
            this.btnCSCliente.Text = "Consultar Clientes";
            this.btnCSCliente.UseVisualStyleBackColor = true;
            this.btnCSCliente.Click += new System.EventHandler(this.btnCSCliente_Click);
            // 
            // lblConsultar
            // 
            this.lblConsultar.AutoSize = true;
            this.lblConsultar.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConsultar.Location = new System.Drawing.Point(14, 21);
            this.lblConsultar.Name = "lblConsultar";
            this.lblConsultar.Size = new System.Drawing.Size(98, 25);
            this.lblConsultar.TabIndex = 64;
            this.lblConsultar.Text = "Consultar";
            // 
            // frmConsultar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 348);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.btnCSPeca);
            this.Controls.Add(this.btnCSdepartamento);
            this.Controls.Add(this.btnCSFornecedor);
            this.Controls.Add(this.btnCSFuncionario);
            this.Controls.Add(this.btnCSCliente);
            this.Controls.Add(this.lblConsultar);
            this.Name = "frmConsultar";
            this.Text = "Consultar";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.Button btnCSPeca;
        private System.Windows.Forms.Button btnCSdepartamento;
        private System.Windows.Forms.Button btnCSFornecedor;
        private System.Windows.Forms.Button btnCSFuncionario;
        private System.Windows.Forms.Button btnCSCliente;
        private System.Windows.Forms.Label lblConsultar;
    }
}