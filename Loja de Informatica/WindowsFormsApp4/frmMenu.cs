﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, EventArgs e)
        {
            frmRegistrar tela = new frmRegistrar();
            tela.Show();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            frmConsultar tela = new frmConsultar();
            tela.Show();

        }

        private void frmMenu01_Load(object sender, EventArgs e)
        {
            
        }

        private void btnOrcamento_Click(object sender, EventArgs e)
        {
            frmSolicitarorçamento tela = new frmSolicitarorçamento();
            tela.Show();
        }
    }
}
